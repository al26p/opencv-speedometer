import json
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
from time import sleep
from collections import deque

import urllib3

battery_voltage = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], maxlen=10)
battery_level = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], maxlen=10)
battery_temp = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0], maxlen=10)


def fetch(url='http://192.168.1.182:8080/sensors.json'):
    http = urllib3.PoolManager()
    data = json.loads(http.request('GET', url).data)
    battery_voltage.append(data['battery_voltage']['data'][0][1][0])
    battery_level.append(data['battery_level']['data'][0][1][0])
    battery_temp.append(data['battery_temp']['data'][0][1][0])
    print(datetime.fromtimestamp(int(data['battery_temp']['data'][0][0] / 1000)))

    # use ggplot style for more sophisticated visuals
    plt.style.use('ggplot')


def live_plotter(x_vec, y_data, data, identifier='', pause_time=2):
    if not data[0]:
        # this is the call to matplotlib that allows dynamic plotting
        plt.ion()
        fig = plt.figure(figsize=(13, 6))
        ax = fig.add_subplot(111)
        # create a variable for the line so we can later update it
        data[0], = ax.plot(x_vec, y_data[0], '-o', alpha=0.8)
        for i, j in zip(x_vec, y_data[0]):
            if j:
                ax.annotate(str(j) + " %", xy=(i, j + 0.5))
        data[1], = ax.plot(x_vec, y_data[1], '-o', alpha=0.8)
        for i, j in zip(x_vec, y_data[1]):
            if j:
                ax.annotate(str(j) + "°C", xy=(i, j + 0.5))
        data[2], = ax.plot(x_vec, y_data[2], '-o', alpha=0.8)
        for i, j in zip(x_vec, y_data[2]):
            if j:
                ax.annotate(str(j) + " V", xy=(i, j + 0.5))


        # update plot label/title
        plt.legend(['battery_level(%)', 'battery_temp(', 'battery_voltage'], loc='upper left')
        plt.show()

    # after the figure, axis, and line are created, we only need to update the y-data
    data[0].set_ydata(y_data[0])
    data[1].set_ydata(y_data[1])
    data[2].set_ydata(y_data[2])

    # this pauses the data so the figure/axis can catch up - the amount of pause can be altered above
    plt.pause(pause_time)

    # return line so we can update it again in the next iteration
    return data


if __name__ == '__main__':
    bat = [[], [], []]
    while True:
        fetch()
        bat = live_plotter([-9, -8, -7, -6, -5, -4, -3, -2, -1, 0], [battery_level, battery_temp, battery_voltage], bat)
        sleep(2)
