import time

import imutils
import numpy as np
from os import system
import cv2
from imutils.video import WebcamVideoStream, VideoStream

# vs = VideoStream(src="rtsp://192.168.1.182:8080/h264_ulaw.sdp", resolution=(320, 240), framerate=15)
vs = WebcamVideoStream(src="rtsp://192.168.1.182:8080/h264_ulaw.sdp")

time.sleep(2)
vs.start()

while True:
    # Capture frame-by-frame
    timer = time.time()
    frame = vs.read()

    frame = imutils.resize(frame, width=500)

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Display the resulting frame
    cv2.imshow('frame', gray)
    system('clear')
    print("elsapsed", time.time()-timer)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    time.sleep(0.001)

# When everything done, release the capture
vs.stop()
cv2.destroyAllWindows()
